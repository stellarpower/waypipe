#!/bin/bash

# Reference used:
# https://saveriomiroddi.github.io/Building-a-debian-deb-source-package-and-publishing-it-on-an-ubuntu-ppa/

package=waypipe
version=0.7.2
licence=mit # expat, From Gitlab
email='stellarpower@googlemail.com' # maintainer

getSources(){
	git clone 'https://gitlab.freedesktop.org/mstoeckl/waypipe.git'
	cd waypipe
	git checkout v${version}
	cd ..
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
pushd "$DIR/debian"

getSources

export DEBFULLNAME='Ben Southall' # Maintainer
dh_make -p ${package}_${version} --single --native --copyright ${licence} --email ${email}

perl -i -pe "s/INITIAL_VERSION/${version}/" debian/changelog # For Ubuntu

source /etc/os-release

if lsb_release -i | grep -i ubuntu ; then
  perl -i -pe "s/INITIAL_OS/${UBUNTU_CODENAME}/" debian/changelog # For Ubuntu
elif lsb_release -i | grep -i debian ; then
  perl -i -pe "s/INITIAL_OS/$(lsb_release -cs)/" debian/changelog # For Debian
fi


# For testing
# debuild    -S       -us           -uc
#    source pkg  nosign source  nosign changes
# now can test with debuild -b to build the binary package
# debuild    -b       -us           -uc

# For a source package for e.g. Launchpad
debuild -S
# And then the binary if building for self
debuild -b

popd
